package com.mitocode.service.impl;

import com.mitocode.model.Paciente;
import com.mitocode.model.Signo;
import com.mitocode.repo.ISignoRepo;
import com.mitocode.service.ISignoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SignoServiceImpl implements ISignoService {

    @Autowired
    private ISignoRepo signoRepo;

    @Override
    public Page<Signo> listarPageable(Pageable pageable) {
        return signoRepo.findAll(pageable);
    }

    @Override
    public Signo registrar(Signo obj) {
        return signoRepo.save(obj);
    }

    @Override
    public Signo modificar(Signo obj) {
        return signoRepo.save(obj);
    }

    @Override
    public List<Signo> listar() {
        return signoRepo.findAll();
    }

    @Override
    public Signo listarPorId(Integer id) {
        return signoRepo.findById(id).orElse(new Signo());
    }

    @Override
    public boolean eliminar(Integer id) {
        signoRepo.deleteById(id);
        return true;
    }

}
