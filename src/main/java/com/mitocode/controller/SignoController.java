package com.mitocode.controller;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Signo;
import com.mitocode.service.ISignoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@PreAuthorize("hasAuthority('ADMIN')")
@RestController
@RequestMapping("/signos")
public class SignoController {

    @Autowired
    private ISignoService signoService;

    @GetMapping
    public ResponseEntity<List<Signo>> listar(){
        List<Signo> lista = signoService.listar();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Signo> listarPorId(@PathVariable("id") Integer id) {
        Signo obj = signoService.listarPorId(id);
        if(obj.getIdSigno() == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
        return new ResponseEntity<Signo>(obj, HttpStatus.OK);
    }

    @GetMapping("/pageable")
    public ResponseEntity<Page<Signo>> listarPageable(Pageable pageable) {
        Page<Signo> signos = signoService.listarPageable(pageable);
        return new ResponseEntity<>(signos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@Valid @RequestBody Signo objeto) {
        Signo signo = signoService.registrar(objeto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(signo.getIdSigno()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<Signo> modificar(@Valid @RequestBody Signo objeto) {
        Signo obj = signoService.modificar(objeto);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        signoService.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
